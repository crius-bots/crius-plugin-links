module gitlab.com/RPGPN/crius-plugin-links

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/jmoiron/sqlx v1.3.1
	gitlab.com/RPGPN/crius-utils v1.5.0
	gitlab.com/RPGPN/criuscommander/v2 v2.4.0
	gitlab.com/ponkey364/golesh-chat v1.1.2
)
